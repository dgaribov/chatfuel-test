package ru.dgaribov.chatfuel;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static ru.dgaribov.chatfuel.Elevator.Direction.*;

/**
 * Лифт.
 * Едет в одну сторону до тех пор, пока есть запрос от пользователя двигаться в эту сторону.
 * Если двигаться дальше не нужно, двигается в другую сторону.
 * Если и в другую сторону двигаться не нужно, стоит на месте
 */
class Elevator extends Thread {

    // Флаг вкл/выкл с помощью которого можно остановить лифт
    private boolean isWorking;

    // Время, необходимое лифту для того, чтобы проехать один этаж
    private int oneFloorMoveMillis;

    // Время, которое проходи с момента открытия дверей, до момента закрытия
    private int doorsClosingMillis;

    // Направление движения лифта
    private Direction direction = NONE;

    // Текущий этаж
    private int currentFloor;

    // Очередь движения вверх
    private Queue<Integer> queueUp;

    // Очередь движения вниз
    private Queue<Integer> queueDown;

    Elevator(int oneFloorMoveMillis, int doorsClosingMillis, int initFloor) {

        currentFloor = initFloor;

        this.oneFloorMoveMillis = oneFloorMoveMillis;
        this.doorsClosingMillis = doorsClosingMillis;

        Comparator<Integer> ascComparator = Comparator.naturalOrder();

        Comparator<Integer> descComparator = Comparator.reverseOrder();

        this.queueUp = new PriorityQueue<>(ascComparator);
        this.queueDown = new PriorityQueue<>(descComparator);

        System.out.printf("Лифт появился на %d этаже%n", initFloor);
        on();

    }

    /**
     * Добавляет этаж в очередь движения лифта
     *
     * Если этаж ниже текущего, то добавляет в очередь вниз
     * Если выше, то в очередь вверх
     * Если этаж совпадает, то просто сообщает об этом
     * @param floor номер этажа, на который заказали лифт
     */
    public void offerFloor(int floor) {

        if (queueDown.contains(floor) || queueUp.contains(floor)) {
            return;
        }

        if (floor == currentFloor) {
            System.out.println("Лифт уже находится на этом этаже");
        } else if (floor > currentFloor) {
            queueUp.add(floor);
        } else {
            queueDown.add(floor);
        }
    }

    /**
     * Механизм движения лифта (в своём потоке), описанный JavaDoc к классу
     */
    public void run() {
        while (isWorking) {
            if (direction == UP) {
                if (queueUp.isEmpty()) {
                    if (!queueDown.isEmpty()) {
                        direction = DOWN;
                    } else {
                        direction = NONE;
                    }
                } else {
                    oneFloorUp();
                }
            } else if (direction == DOWN) {
                if (queueDown.isEmpty()) {
                    if (!queueUp.isEmpty()) {
                        direction = UP;
                    } else {
                        direction = NONE;
                    }
                } else {
                    oneFloorDown();
                }
            } else {
                if (!queueDown.isEmpty()) {
                    direction = DOWN;
                } else if (!queueUp.isEmpty()) {
                    direction = UP;
                } else {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        // логируем ошибку
                    }
                }
            }
        }

    }

    private void on() {
        isWorking = TRUE;
        System.out.println("Лифт запущен");
    }

    public void off() {
        isWorking = FALSE;
        System.out.println("Лифт остановлен");
    }

    /**
     * Симулирует движение лифта на один этаж вверх, останавливая поток на время, необходимое лифту на преодаление этажа
     * Информирует пользователя о своих перемещениях
     * Открывает двери на запрошенных этажах и удаляет их из очереди.
     */
    private void oneFloorUp() {
        takeTimeForOneFloorMove();
        currentFloor += 1;
        int nextFloor = queueUp.peek();
        if (currentFloor == nextFloor) {
            queueUp.poll();
            System.out.format("Лифт прибыл на %d этаж%n", currentFloor);
            openCloseDoors();
        } else {
            System.out.format("Лифт проехал %d этаж%n", currentFloor);
        }
    }

    /**
     * Симулирует движение лифта на один этаж вниз
     * См. выше
     */
    private void oneFloorDown() {
        takeTimeForOneFloorMove();
        currentFloor -= 1;
        int nextFloor = queueDown.peek();
        if (currentFloor == nextFloor) {
            queueDown.poll();
            System.out.format("Лифт прибыл на %d этаж%n", currentFloor);
            openCloseDoors();
        } else {
            System.out.format("Лифт проехал %d этаж%n", currentFloor);
        }
    }

    private void takeTimeForOneFloorMove() {
        try {
            Thread.sleep(oneFloorMoveMillis);
        } catch (InterruptedException e) {
            // логируем ошибку
        }
    }

    private void openCloseDoors() {
        System.out.println("Лифт открыл двери");
        try {
            Thread.sleep(doorsClosingMillis);
        } catch (InterruptedException e) {
            // логируем ошибку
        }
        System.out.println("Лифт закрыл двери");
    }

    public enum Direction {
        UP, DOWN, NONE
    }
}
