package ru.dgaribov.chatfuel;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class App {

    public static void main(String[] args) {

        // Количество этажей
        int floorsCount;
        // Высота этажа в см
        int floorHeight;
        // Скорость лифта в м/с
        float elevatorSpeed;
        // Время закрытия дверей в секундах
        int doorsClosingMillis;

        Scanner in = new Scanner(System.in);

        System.out.println("Введите количество этажей от 5 до 20");
        floorsCount = in.nextInt();
        while (floorsCount < 5 || floorsCount > 20) {
            System.out.println("Введено неверное количество эатажей, попробуйте ещё раз");
            floorsCount = in.nextInt();
        }

        System.out.println("Введиите высоту этажа в см");
        floorHeight = in.nextInt();
        while (floorHeight < 1) {
            System.out.println("Введена неверная высота этажа, попробуйте еще раз");
            floorHeight = in.nextInt();
        }

        System.out.println("Введиите скорость лифта в м/с");
        elevatorSpeed = in.nextFloat();
        while (elevatorSpeed <= 0) {
            System.out.println("Введена неверная скорость лифта, попробуйте еще раз");
            elevatorSpeed = in.nextInt();
        }

        System.out.println("Введиите время закрытия дверей в секундах");
        doorsClosingMillis = in.nextInt();
        while (doorsClosingMillis <= 0) {
            System.out.println("Введено неверное время закрытия дверей лифта, попробуйте еще раз");
            doorsClosingMillis = in.nextInt();
        }
        doorsClosingMillis *= 1000;

        int oneFloorMillis = (int) (((floorHeight/100) / elevatorSpeed) * 1000);

        int initFloor = ThreadLocalRandom.current().nextInt(1, floorsCount + 1);

        // Запускаем лифт
        Elevator elevator = new Elevator(oneFloorMillis, doorsClosingMillis, initFloor);
        elevator.start();

        System.out.println("Введите P для вызова лифта в подъезд");
        System.out.printf("Введите число от 1 до %d для нажатия кнопки внутри лифта%n", floorsCount);
        System.out.println("Введите Q для остановки лифта и входа из программы");

        String command;
        while (in.hasNext()) {
            command = in.next();
            switch (command) {
                case "P" : {
                    elevator.offerFloor(1);
                    break;
                } case "Р" : {
                    elevator.offerFloor(1);
                    break;
                } case "Q" : {
                    elevator.off();
                    System.exit(0);
                    break;
                } default: {
                    int floor;
                    try {
                        floor = Integer.parseInt(command);
                    } catch (Exception ex) {
                        System.out.println("Введен неверная команда");
                        break;
                    }
                    if (floor < 1 || floor > floorsCount) {
                        System.out.println("Введен неверный номер этажа");
                        break;
                    }
                    elevator.offerFloor(floor);
                }
            }
        }
    }
}
